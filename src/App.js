import React from 'react';
import Names from './components/Names';
import Surnames from './components/Surnames';

class App extends React.Component {
  render() {
    const names = ['Clifford', 'Russel', 'Michael', 'Dennis', 'Andre'];
    const surnames = ['Smith', 'Simmons', 'Diamond', 'Coles', 'Benjamin'];
    const renderNames = (names) => {
    return names.map((name, i) => {
      return <div>
      <Names name={name} />
      <Surnames surname={surnames[i]} />
      </div>
    })
  }
    return (
      <div className="App">
        {renderNames(names)}
      </div>
    );
  }
}

export default App;
